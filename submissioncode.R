libraries<-c('readr', 'ggplot2', 'caret', 'corrplot', 'plyr', 'dplyr', 'ROSE', 'tidyr', 'pls', 'C50', 'gbm', 'kernlab', 'glmnet', 'nnet',
             'data.table', 'xgboost', 'openxlsx')
sapply(libraries, library, character.only = TRUE) # load the required packages


TrainingData <- read_csv("E:/projects/Data driven/Blood Tranfusion Competition/TrainingData.csv")

TestData <- read_csv("E:/projects/Data driven/Blood Tranfusion Competition/TestData.csv")


names(TrainingData)[names(TrainingData) == "Number of Donations"] <- 'Donations'

names(TrainingData)[names(TrainingData) == "Months since Last Donation"] <- 'Last.Donation'

names(TrainingData)[names(TrainingData) == "Months since First Donation"] <- 'First.Donation'

names(TrainingData)[names(TrainingData) == "Total Volume Donated (c.c.)"] <- 'Total.Volume'

names(TrainingData)[names(TrainingData) == "Made Donation in March 2007"] <- 'March.2007'
#####################
names(TestData)[names(TestData) == "Number of Donations"] <- 'Donations'

names(TestData)[names(TestData) == "Months since Last Donation"] <- 'Last.Donation'

names(TestData)[names(TestData) == "Months since First Donation"] <- 'First.Donation'

names(TestData)[names(TestData) == "Total Volume Donated (c.c.)"] <- 'Total.Volume'

names(TestData)[names(TestData) == "Made Donation in March 2007"] <- 'March.2007'



TrainingData$March.2007<-as.character((TrainingData$March.2007))
TrainingData$March.2007[TrainingData$March.2007 == '0'] <- "Not.Donate"
TrainingData$March.2007[TrainingData$March.2007 == '1'] <- "Donate"



table(TrainingData$March.2007)
#Remove X1 this seems to be the primary key from the database
TrainingData1<-select(TrainingData, -X1)

#Add a columen for the frequency of donation
TrainingData1<-mutate(TrainingData1,Frequency.Donations=First.Donation/Donations)
#Introduction of the line above reduced the accuracy to 0.8081395.
library(dprep)
#Let us drop one of the correlated vriables and retry
TrainingData<-select(TrainingData1, -Donations)
outlier.test(TrainingData1)
View(TrainingData1)
TrainingData1[-which(Damp==F),]
#The accuracy increases after dropping the column above

#TrainingData$Total.Volume<-sin(TrainingData$Total.Volume) #reduces the accuracy

#Likelihood to donate this month
TrainingData1<-mutate(TrainingData1, Donate.March=Frequency.Donations - Last.Donation) #The accuracy remains 0.8081395

#Try removing the frequency of donations
# TrainingData<-select(TrainingData, -Frequency.Donations) #reduces to 0.7906977

TestData1 <- select(TestData, -X1)
TestData1<-mutate(TestData1,Frequency.Donations=First.Donation/Donations)
TestData1<-mutate(TestData1, Donate.March=Frequency.Donations - Last.Donation)
colnamesy <- c(names(TrainingData1), names(TestData1))
TrainingData1$March.2007<-as.factor((TrainingData1$March.2007))
set.seed(113)
# cutoff = createDataPartition(TrainingData1$March.2007, p = 0.7, list = FALSE)
# trainingdata<-TrainingData1[cutoff,]
# testdata<-TrainingData1[-cutoff,]



gbmGrid <-  expand.grid(interaction.depth = 1,
                        n.trees = seq(100, 450, by =40),
                        shrinkage = 0.01,
                        n.minobsinnode = c(10, 20, 30))

nrow(gbmGrid)
ImbalancedModel<- caret::train(March.2007~., data = TrainingData1, method = "gbm",
                               preProcess = c("pca", "YeoJohnson"),
                               metric="logLoss",
                               trControl= trainControl(method = "repeatedcv",
                                                       number = 10,
                                                       repeats = 10,
                                                       verboseIter = FALSE,
                                                       classProbs=TRUE,
                                                       summaryFunction=mnLogLoss),
                               tuneGrid = gbmGrid)


print(ImbalancedModel)

ans <- data.frame(predict(ImbalancedModel, newdata = TestData1, type = "prob"))


ggplot(ImbalancedModel) + theme(legend.position = "top")

# Submission <- read_csv("E:/projects/Data driven/Blood Tranfusion Competition/BloodDonationSubmissionFormat.csv")
# names(Submission)
# Submission <- cbind(Submission$X1 , ans$Donate ) 
# colnames(Submission)<-c("X1", "Made Donation in March 2007")
# Submission<-data.frame(Submission)
# 
# 
# write.xlsx(Submission, file = "submission.xlsx")
